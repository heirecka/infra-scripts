#!/bin/sh

set -e

# First param is the tarball, 2nd is the checksum
VerifySha()
{
    test_sum=$(awk -v myvar="$1" '$2==myvar {for(i=1; i<=1; i++) { print $1; exit}}' $2)
    calculated_sum=$(sha1sum $1 | awk '{print $1}' -)
    if [[ "$test_sum" == "$calculated_sum" ]] ; then
        return 0
    else
        return 1
    fi
}

baseurl="http://dev.exherbo.org/stages/"
rootfs="exherbo-x86_64-pc-linux-gnu-current.tar.xz"
checksum="sha1sum"

# Create working directory, keep a copy of busybox
mkdir /work
cp /bin/busybox /work
mkdir /work/rootfs
cd /work/rootfs


echo "Downloading rootfs"
wget -c "${baseurl}/${rootfs}" "${baseurl}/${checksum}"
if VerifySha ${rootfs} ${checksum} ; then
    echo "Checksum is OK"
else
    echo "Checksum is NOT OK"
    return 1
fi

echo "Unpacking rootfs"
EXTRACT_UNSAFE_SYMLINKS=1 tar --exclude "./dev" --exclude "./proc" --exclude "./sys" --exclude "./etc/hosts" --exclude "./etc/hostname" --exclude "./etc/resolv.conf" -xf ${rootfs}
/work/busybox rm -f ${rootfs} ${checksum}

echo "Cleaning up"
cd /
/work/busybox rm -rf /bin /build.sh /etc /home /root /tmp /usr /var || true
/work/busybox mv /work/rootfs/etc/* /etc || true
/work/busybox rm -rf /work/rootfs/etc
/work/busybox mv /work/rootfs/* /
/work/busybox rm -rf /work

echo "Contents of rootfs:"
ls -lah

echo "Update the rootfs"
source /etc/profile
eclectic env update

chgrp paludisbuild /dev/tty

# disable tests
echo '*/* build_options: -recommended_tests' >> /etc/paludis/options.conf

cave sync
eclectic news read new
cave resolve world -cx
cave purge -x
cave fix-linkage -x
eclectic config accept-all

# reenable tests again
sed '/\*\/\* build_options: -recommended_tests/d' -i /etc/paludis/options.conf

echo "Cleaning up again"
rm -f /root/.bash_history
rm -Rf /tmp/*
rm -Rf /var/tmp/paludis/build/*
rm -Rf /var/cache/paludis/distfiles/*
rm -Rf /var/log/paludis/*
rm -f /var/log/paludis.log

