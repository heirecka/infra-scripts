#!/bin/bash

set -e

echo "Building base container..."
pushd "$(dirname "${BASH_SOURCE[0]}")"/base
docker build --no-cache --pull -t exherbo/exherbo_base .
docker push exherbo/exherbo_base
popd

echo "Building CI container..."
pushd "$(dirname "${BASH_SOURCE[0]}")"/ci
docker build --no-cache --pull -t exherbo/exherbo_ci .
docker push exherbo/exherbo_ci
popd

