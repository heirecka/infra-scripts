#!/bin/sh

set -e

echo "Setting up CI environment"
source /etc/profile
eclectic env update

chgrp paludisbuild /dev/tty

echo "sys-apps/paludis ruby" >> /etc/paludis/options.conf

echo '*/* build_options: -recommended_tests' >> /etc/paludis/options.conf
cave resolve paludis ruby-elf -x
sed '/\*\/\* build_options: -recommended_tests/d' -i /etc/paludis/options.conf

echo "Downloading build scripts"
cd /usr/local/bin
wget -c https://git.exherbo.org/infra-scripts.git/plain/continuous-integration/gitlab/buildtest
wget -c https://git.exherbo.org/infra-scripts.git/plain/continuous-integration/gitlab/handle_confirmations
wget -c https://git.exherbo.org/exherbo-dev-tools.git/plain/mscan2.rb
chmod +x buildtest handle_confirmations mscan2.rb

echo "Cleaning up again"
rm -f /build.sh
rm -f /root/.bash_history
rm -Rf /tmp/*
rm -Rf /var/tmp/paludis/build/*
rm -Rf /var/cache/paludis/distfiles/*
rm -Rf /var/log/paludis/*
rm -f /var/log/paludis.log

